# Utilita

Provides commandline utilities to your terminal

## Set up
To make the utilities provided by this app usable in your terminal run

    export PATH=$PATH:/opt/click.ubuntu.com/utilita.fuseteam/current/bin

To make it persistent add the above line in your ~/.bashrc

## License

Copyright (C) 2020  Jonatan Hatakeyama Zeidler
Copyright (C) 2021  Rahammetoela Toekiman

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
